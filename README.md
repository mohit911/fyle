# Fyle

## 1. API TO GET BANK DETAILS
	Format:
	http://3.84.245.130:8000/bank_and_branch_details/<ifsc_code>/?&<query params>

	Example:
	curl -X GET 'http://3.84.245.130:8000/bank_and_branch_details/ABHY0065001/?fields=bank_id,bank_name&=' -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTcwOTU5MDEzLCJqdGkiOiIxMzkxYjc0NzM0NmI0YWFlYTY0OTEzZTY3MTE0OGJiYiIsInVzZXJfaWQiOjF9.6iol-2TCBDkJYlyKa35MPNKaZ0NbEqfpOK94m80E8sU'


## 2. API TO GET BRANCH DETAILS
	Format:
	http://3.84.245.130:8000/bank_and_branch_details/?<query params>

	Example:
	curl -X GET 'http://3.84.245.130:8000/bank_and_branch_details/?limit=2&offset=0&bank_name=ABHYUDAYA%20COOPERATIVE%20BANK%20LIMITED&city=MUMBAI&fields=ifsc,branch,address,city,district,state' -H 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTcwOTU5MDEzLCJqdGkiOiIxMzkxYjc0NzM0NmI0YWFlYTY0OTEzZTY3MTE0OGJiYiIsInVzZXJfaWQiOjF9.6iol-2TCBDkJYlyKa35MPNKaZ0NbEqfpOK94m80E8sU'


## 3. API TO GENERATE TOKEN
	curl -X POST http://3.84.245.130:8000/api/token/ -H 'Content-Type: application/json' -d '{"username": "admin","password": "django123"}'