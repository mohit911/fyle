from django.db import models

# Create your models here.


class Bank(models.Model):
    """Docstring for Bank."""

    name = models.CharField(
        max_length=49)

    def __str__(self):
        """Unicode method to display Object in Admin."""
        return str(self.name)


class Branch(models.Model):
    """Docstring for Branches."""

    ifsc = models.CharField(
        max_length=11,
        primary_key=True,
        unique=True)

    bank = models.ForeignKey(
        Bank,
        on_delete=models.PROTECT)

    branch = models.CharField(
        max_length=74)

    address = models.CharField(
        max_length=195)

    city = models.CharField(
        max_length=50)

    district = models.CharField(
        max_length=50)

    state = models.CharField(
        max_length=26)

    def __str__(self):
        """Unicode method to display Object in Admin."""
        return str(self.bank)


class BankBranches(models.Model):
    bank_id = models.IntegerField()

    ifsc = models.CharField(
        max_length=11,
        primary_key=True,
        unique=True)

    bank_name = models.CharField(
        max_length=49)

    branch = models.CharField(
        max_length=74)

    address = models.CharField(
        max_length=195)

    city = models.CharField(
        max_length=50)

    district = models.CharField(
        max_length=50)

    state = models.CharField(
        max_length=26)

    class Meta:
        managed = False
        db_table = 'bank_branches'
