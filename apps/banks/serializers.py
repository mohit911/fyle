from rest_framework import serializers
from .models import BankBranches


class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    """A ModelSerializer that takes an additional `fields` argument that controls which fields should be displayed."""

    def __init__(self, *args, **kwargs):
        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        fields = self.context['request'].query_params.get('fields')
        if fields:
            fields = fields.split(',')
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class BankBranchSerializer(DynamicFieldsModelSerializer, serializers.ModelSerializer):
    """Docstring for BankBranchSerializer."""

    class Meta:
        model = BankBranches
        fields = ['bank_id','bank_name', "ifsc", "branch", "address", "city", "district", "state"]
