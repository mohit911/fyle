from .models import BankBranches
from .serializers import BankBranchSerializer

from rest_framework.pagination import LimitOffsetPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets

from url_filter.integrations.drf import DjangoFilterBackend

# Create your views here.


class BankBranchDetails(viewsets.ModelViewSet):
    """Viewset to get/filter details from  BankBranches(View Table)."""

    serializer_class = BankBranchSerializer
    queryset = BankBranches.objects.all()
    pagination_class = LimitOffsetPagination
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['ifsc', 'bank_name', 'city']
    permission_classes = [IsAuthenticated]
